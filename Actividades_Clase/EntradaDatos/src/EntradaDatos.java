import javax.swing.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class EntradaDatos {

    public static void main(String[] args) {

        EntradaScanner();
        //EntradaInputDialog();
        //EntradaBufferedReader();

    }

    static void EntradaBufferedReader(){

        // Inicialización de objetos para entrada de datos.
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader Teclado = new BufferedReader(entrada);

        // Declaración de variables
        int annio = 0;
        double altura = 0.0;
        float peso = 0.0f;
        char inicialApellido = '\0';
        String nombre = "";
        String dia = "";

        System.out.println("\n¿Qué día es hoy?");
        try {

            dia = Teclado.readLine();

        }catch (Exception e){

            System.out.println("\nIngresaste un tipo equivocado");
            System.out.println("Error: " + e);

        }

        System.out.println("\n¿Qué año es?");
        try {

            annio = Integer.parseInt(Teclado.readLine());

        }catch (Exception e){

            System.out.println("\nIngresaste un tipo equivocado");
            System.out.println("Error: " + e);

        }

        System.out.println("\n¿Cuánto mides?");
        try {

            altura = Double.parseDouble(Teclado.readLine());

        }catch (Exception e){

            System.out.println("\nIngresaste un tipo equivocado");
            System.out.println("Error: " + e);

        }

        System.out.println("\n¿Cuánto pesas?");
        try {

            peso = Float.parseFloat(Teclado.readLine());

        }catch (Exception e){

            System.out.println("\nIngresaste un tipo equivocado");
            System.out.println("Error: " + e);

        }

        System.out.println("\n¿Cuál es la inicial de tu apellido?");
        try {

            inicialApellido = Teclado.readLine().charAt(0);

        }catch (Exception e){

            System.out.println("\nIngresaste un tipo equivocado");
            System.out.println("Error: " + e);

        }

        System.out.println("\n¿Cuál es tu nombre?");
        try {

            nombre = Teclado.readLine();

        }catch (Exception e){

            System.out.println("\nIngresaste un tipo equivocado");
            System.out.println("Error: " + e);

        }

        System.out.println("\n¡Hola " + nombre + " " + inicialApellido + ". !, bonito " + dia + " de " + annio + " según mis registros, pesas " + peso + " [Kg] y mides " + altura + " [m] ");

    }

    static void EntradaInputDialog(){

        // Declaración de variables
        int annio;
        double altura;
        float peso;
        char inicialApellido;
        String nombre;
        String dia;

        // Reasignación de variables, mensajes al usuario y entrada de datos.
        dia = JOptionPane.showInputDialog(null, "¿Qué día es hoy?");
        annio = Integer.parseInt(JOptionPane.showInputDialog(null, "¿Qué año es?"));
        altura = Double.parseDouble(JOptionPane.showInputDialog(null, "¿Cuánto mides?"));
        peso = Float.parseFloat(JOptionPane.showInputDialog(null, "¿Cuánto pesas?"));
        inicialApellido = JOptionPane.showInputDialog(null, "¿Cuál es la inicial de tu apellido?").charAt(0);
        nombre = JOptionPane.showInputDialog(null, "¿Cuál es tu nombre?");

        // Salida de datos en cuadro de diálogo.
        JOptionPane.showMessageDialog(null, "\n¡Hola " + nombre + " " + inicialApellido + ". !, bonito " + dia + " de " + annio + " según mis registros, pesas " + peso + " [Kg] y mides " + altura + " [m] ");

    }

    static void EntradaScanner(){

        // Declaración de variables
        int annio;
        double altura;
        float peso;
        char inicialApellido;
        String nombre;
        String dia;

        Scanner Teclado = new Scanner(System.in);

        // Enteros
        System.out.println("\n¿Qué día es hoy?");
        dia = Teclado.nextLine();
        System.out.println("\n¿Qué año es?");
        annio = Teclado.nextInt();
        System.out.println("\n¿Cuánto mides?");
        altura = Teclado.nextDouble();
        System.out.println("\n¿Cuánto pesas?");
        peso = Teclado.nextFloat();
        System.out.println("\n¿Cuál es la inicial de tu apellido?");
        inicialApellido = Teclado.next().charAt(0);
        System.out.println("\n¿Cuál es tu nombre?");
        nombre = Teclado.next();

        System.out.println("\n¡Hola " + nombre + " " + inicialApellido + ". !, bonito " + dia + " de " + annio + " según mis registros, pesas " + peso + " [Kg] y mides " + altura + " [m] ");

    }

}
