public class Cuadrado extends Figura{

    private float lado;

    public Cuadrado(float lado) {
        this.lado = lado;

        System.out.println("CUADRADO:");
        System.out.println("Lado: " + getLado());
        System.out.println("Perímetro: " + obtenerPerimetro());
        System.out.println("Área: " + obtenerArea());

    }

    public float getLado() {
        return lado;
    }

    @Override
    float obtenerArea(){
        return (float)Math.pow(lado,2);
    }

    @Override
    float obtenerPerimetro(){
        return lado * 4;
    }

}
