public class Circulo extends Figura{

    private float radio;

    public Circulo(float radio) {
        this.radio = radio;

        System.out.println("Circulo:");
        System.out.println("Radio: " + getRadio());
        System.out.println("Perímetro: " + obtenerPerimetro());
        System.out.println("Área: " + obtenerArea());

    }

    public float getRadio() {
        return radio;
    }

    @Override
    float obtenerArea(){
        return (float)(Math.PI * Math.pow(radio,2));
    }

    @Override
    float obtenerPerimetro(){
        return (float)(2 * Math.PI * radio);
    }

}