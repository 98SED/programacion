public class Triangulo {

    private float h, co, ca;

    public Triangulo(float h, float co, float ca) {
        this.h = h;
        this.co = co;
        this.ca = ca;

        System.out.println("CUADRADO:");
        System.out.println("Cateto Opuesto: " + getCo());
        System.out.println("Cateto Adyacente: " + getCa());
        System.out.println("Hipotenusa: " + getH());
        System.out.println("Perímetro: " + obtenerPerimetro());
        System.out.println("Área: " + obtenerArea());

    }

    public float getH() {
        return h;
    }

    public float getCo() {
        return co;
    }

    public float getCa() {
        return ca;
    }

    @Override
    float obtenerArea(){
        return (co * ca) / 2;
    }

    @Override
    float obtenerPerimetro(){
        return co + ca + h;
    }

}
