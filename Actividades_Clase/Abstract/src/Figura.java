public abstract class Figura {

    abstract float obtenerArea();

    abstract float obtenerPerimetro();
}
