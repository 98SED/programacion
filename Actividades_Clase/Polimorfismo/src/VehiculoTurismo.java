public class VehiculoTurismo extends Vehiculo {

    private int numeroPuertas;

    public VehiculoTurismo(String matricula, String marca, String modelo, int numeroPuertas) {
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
        this.numeroPuertas = numeroPuertas;
    }

    @Override
    public String mostrarDatos(){
        return (" | Marca: " + marca + " | Modelo: " + modelo + " | Número de Puertas: " + numeroPuertas + " | Matricula: " + matricula);
    }

}
