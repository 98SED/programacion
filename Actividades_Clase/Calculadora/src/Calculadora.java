import java.util.Scanner;

public class Calculadora {

    public static void main(String... args){

        // Declaración de variables estructuradas
        Scanner entradaDatos = new Scanner(System.in);
        String mensaje = "\n¡Hola Usuario!, este programa realiza cálculos básicos, a continuación deberás ingresar dos números enteros para operarlos:\n";

        // Declaración de variables primitivas
        int n1;
        int n2;
        double resultado;



        // Mensaje
        System.out.println(mensaje);

        // Entrada de datos
        System.out.println("Ingrese un número:");
        n1 = entradaDatos.nextInt();
        System.out.println("Ingrese otro número:");
        n2 = entradaDatos.nextInt();

        // Resultados y reasignación de variables primitivas

        // Suma
        resultado = n1 + n2;
        System.out.println(n1 + " + " + n2 + " = " + (int)resultado);

        // Multiplicación
        resultado = n1 * n2;
        System.out.println(n1 + " × " + n2 + " = " + (int)resultado);

        // Resta
        resultado = n1 - n2;
        System.out.println(n1 + " – " + n2 + " = " + (int)resultado);

        // División
        resultado = (double)n1 / n2;
        System.out.println(n1 + " ÷ " + n2 + " = " + resultado);

    }

}

/*
 * ANOTACIONES:
 *
 * Tipos primitivos:
 *
 * Numéricos: boolean, byte, short, int, long, float, double
 * Carácter: char
 *
 *
 * Tipos estructurados:
 *
 * String
 * Arreglos
 * Clases
 */
