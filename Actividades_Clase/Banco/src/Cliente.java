public class Cliente {

    private int id;
    private String nombre;
    private String telefono;
    private String correo;

public Cliente(int id, String nombre, String telefono, String correo){

    this.id = id;
    this.nombre = nombre;
    this.telefono = telefono;
    this.correo = correo;

}

    void setNombre(String x){

        nombre = x;

    }

    void setTelefono(String x){

        telefono = x;

    }

    void setCorreo(String x){

        correo = x;

    }

    int getID(){

        return id;

    }

    String getNombre(){

        return nombre;

    }

    String getTelefono(){

        return telefono;

    }

    String getCorreo(){

        return correo;

    }

}
