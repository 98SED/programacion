public class Menu {

    Directorio directorio = new Directorio();

    void añadirPrueba(){

        Cliente prueba = new Cliente(1, "R2", "4536284533", "r2d2@galactinmailservices.com");

        directorio.addCliente(prueba);

    }


    public void menu() {

        int opt;

        do {

            System.out.println("¡Bienvenido a JBank!, Selecciona una de las opciones de nuestro menu:\n" +
                    "1. Registrarse\n" +
                    "2. Depositar efectivo\n" +
                    "3. Retirar efectivo\n" +
                    "4. Consultar Saldo\n" +
                    "5. Consultar datos\n" +
                    "6. Modificar datos\n" +
                    "7. Eliminar tu cuenta\n" +
                    "8. Consultar Usuarios\n" +
                    "9. Cerrar Sesión\n");
            opt = Teclado.LeerEntero();

            switch (opt) {

                case 1:
                    directorio.generarCliente();
                    break;
                case 2:
                    System.out.println("Cajero en mantenimiento, intenta con otra opción");
                    break;
                case 3:
                    System.out.println("Cajero en mantenimiento, intenta con otra opción");
                    break;
                case 4:
                    System.out.println("Cajero en mantenimiento, intenta con otra opción");
                    break;
                case 5:
                    System.out.println("Cajero en mantenimiento, intenta con otra opción");
                    break;
                case 6:

                    if (directorio.directorio.isEmpty()){
                        System.out.println("No se encontró el cliente indicado en la BD.");
                    }else {

                        System.out.println("Ingresa un ID valido para modificar su información: ");
                        directorio.listarCliente();
                        int id = Teclado.LeerEntero();

                        System.out.println("Ingresa el nuevo correo eletrónico:");
                        String email = Teclado.LeerCadena();

                        System.out.println("Ingresa el nuevo teléfono:");
                        String tel = Teclado.LeerCadena();

                        directorio.modCliente(id, tel, email);

                    }

                    break;
                case 7:

                    if (directorio.directorio.isEmpty()){
                        System.out.println("No se encontró el cliente indicado en la BD.");
                    }else {

                        System.out.println("Ingresa un ID valido para eliminar su información: ");
                        directorio.listarCliente();
                        int id = Teclado.LeerEntero();
                        directorio.subCliente(id);

                    }


                    break;
                case 8:
                    directorio.listarCliente();
                    break;
                case 9:
                    break;
                default:
                    System.out.println("Ingresa una opción valida");
                    break;

            }

        }while(opt!=9);

    }

}