import java.util.ArrayList;

public class Directorio {

    ArrayList<Cliente> directorio = new ArrayList<Cliente>();

    void generarCliente() {

        int id;
        String nombre, correo, telefono;

        System.out.println("Ingrese su ID:");
        // cliente.setID(Teclado.LeerEnteroS());

        id = Teclado.LeerEntero();

        System.out.println("Ingresa tu nombre para abrir una cuenta:");
        // cliente.setNombre(Teclado.LeerCadena());

        nombre = Teclado.LeerCadena();

        System.out.println("Proporciona un número telefónico y un correo en caso de tener que contactarte:" +
                "Teléfono:");
        // cliente.setTelefono(Teclado.LeerCadena());

        telefono = Teclado.LeerCadena();

        System.out.println("Correo electrónico:");
        //cliente.setCorreo(Teclado.LeerCadena());

        correo = Teclado.LeerCadena();

        Cliente cliente = new Cliente(id, nombre, telefono, correo);
        addCliente(cliente);

    }

    void addCliente(Cliente cliente){

        directorio.add(cliente);

    }

    void modCliente(int id, String tel, String email){

        boolean encontrado = false;


        for(Cliente cliente:directorio){

            if (cliente.getID() == id){

                encontrado = true;
                System.out.println("Cliente encontrado");

                cliente.setCorreo(email);
                cliente.setTelefono(tel);

            }

        }

        if (!encontrado){

            System.out.println("No se encontró el cliente indicado en la BD.");

        }

    }

    //void subCliente(Cliente cliente){
    void subCliente(int id){

        boolean encontrado = false;

        for(Cliente cliente:directorio){

            if (cliente.getID() == id){

                encontrado = true;

                directorio.remove(cliente);
                break;

            }

        }

        if (!encontrado){

            System.out.println("No se encontró el cliente indicado en la BD.");

        }

    }

    void listarCliente(){

        if (directorio.isEmpty()) {

            System.out.println("En este momento no hay clientes en la BD");

        }else {
            for (Cliente cliente :
                    directorio) {
                System.out.println(cliente.getID() + " | " + cliente.getNombre() + " | " + cliente.getCorreo() + " | " + cliente.getTelefono());
            }
        }

    }
}
