public class Producto {

    private String sku;
    private String nombre;
    private int cantidad;
    static int contador;

    public Producto(String sku, String nombre, int cantidad) {
        this.sku = sku;
        this.nombre = nombre;
        this.cantidad = cantidad;
    }
}
