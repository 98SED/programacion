import java.util.ArrayList;

public class Inventario {

    ArrayList<Producto> productos = new ArrayList<>();


    public void anadirProducto() {

        int sku, cantidad;
        int min = 5;
        int max = Integer.MAX_VALUE;
        double precio;
        String nombre, descripcion;

        sku = (int)Math.floor(Math.random()*(max-min+1)+min);
        System.out.println("Ingrese el nombre del producto " + sku + ":");
        nombre = Teclado.LeerCadena();
        System.out.println("Ingrese la descripción del producto " + sku + ":");
        descripcion = Teclado.LeerCadena();
        System.out.println("Ingrese la cantidad del producto " + sku + ":");
        cantidad = Teclado.LeerEnteroS();
        System.out.println("Ingrese el precio del producto " + sku + ":");
        precio = Teclado.LeerDouble();

        Producto producto = new Producto(sku, nombre, descripcion, cantidad, precio);
        productos.add(producto);

    }

    /*public void eliminarProducto(){

        int sku;
        boolean validador = false;

        System.out.println("Ingrese un sku valido para eliminar un producto");
        sku = Teclado.LeerEntero();

        for (Producto producto:productos){

            if (producto.getSku() == sku){

                validador = true;
                productos.remove(producto);

                break;

            }

        }

        if(!validador){
            System.out.println("El sku " + sku + " no se encontro en la BD, ingrese un sku valido");
        }

    }*/

    private int[] contarProductos() {

//        int total = 0, agotados = 0, existentes = 0;
        int conteo[] = {0,0,0};

        for (Producto producto :
                productos) {
            conteo[0]+=1;
            if (producto.getCantidad() == 0) {
                conteo[2]+=1;
            } else {
                conteo[1]+=1;
            }
        }

        return conteo;

    }

    public void listarProductos() {

        int ciclo = 0;

        if (productos.isEmpty()) {

            System.out.println("En este momento no hay productos en la BD");

        } else {

            int opt;
            int opt2;
            int conteo[] = contarProductos();

            System.out.println(" ╔════════════════════════════════════════════════════════════════════════════════════════════════════════════╗\n" +
                    " ║ ========================================================================================================== ╝\n" +
                    " ║ Hay un total de " + conteo[0] + " productos en el inventario. " + conteo[1] + " en existencia y " + conteo[2] + " agotados\n" +
                    " ║ ========================================================================================================== ╗\n" +
                    " ║ \n");

            do {

                System.out.println("\n ║ Ingresa el SKU de un producto o alguna de las siguientes opciones:\n" +
                        " ║ \n" +
                        " ║ 1. Listar todos los productos\n" +
                        " ║ 2. Listar solo los productos en existencia\n" +
                        " ║ 3. Listar solo los productos agotados\n" +
                        " ║ 4. Regresar\n" +
                        " ║ 5. Salir");

                do {
                    opt = Teclado.LeerEntero();


                    if (opt != 1 && opt != 2 && opt != 3 && opt != 4 && opt != 5) {

                        for (Producto producto :
                                productos) {
                            if (producto.getSku() == opt) {

                                do {

                                    System.out.println(" ║ Selecciona alguna de las siguientes opciones para el producto: " + opt + "\n" +
                                            " ║ \n" +
                                            " ║ 1. Modificar nombre\n" +
                                            " ║ 2. Modificar descripción\n" +
                                            " ║ 3. Modificar precio\n" +
                                            " ║ 4. Aumentar o disminuir cantidad\n" +
                                            " ║ 5. Eliminar\n" +
                                            " ║ 6. Regresar\n" +
                                            " ║ 7. Salir\n");

                                    do {
                                        opt2 = Teclado.LeerEntero();
                                        switch (opt2) {

                                            case 1:
                                                String nombre;
                                                System.out.println(" ║ Ingrese el nuevo nombre del producto seleccionado");
                                                nombre = Teclado.LeerCadena();
                                                producto.setNombre(nombre);
                                                System.out.println(" ║ El nuevo nombre es: " + producto.getNombre());
                                                break;
                                            case 2:
                                                String descripcion;
                                                System.out.println(" ║ Ingrese la nueva descripción del producto seleccionado");
                                                descripcion = Teclado.LeerCadena();
                                                producto.setDescripcion(descripcion);
                                                System.out.println(" ║ La nueva descripción es: " + producto.getDescripcion());
                                                break;

                                            case 3:
                                                double precio;
                                                System.out.println(" ║ Ingrese el nuevo precio del producto seleccionado");
                                                precio = Teclado.LeerDouble();
                                                producto.setPrecio(precio);
                                                System.out.println(" ║ El nuevo precio es: " + producto.getPrecio());
                                                break;

                                            case 4:
                                                int cantidad;
                                                System.out.println(" ║ Ingrese una cantidad para aumentar o disminuir las existencias del producto seleccionado");
                                                cantidad = Teclado.LeerEntero();
                                                producto.addCantidad(cantidad);
                                                System.out.println(" ║ La nueva cantidad es: " + producto.getCantidad());
                                                break;

                                            case 5:
                                                productos.remove(producto);
                                                System.out.println(" ║ El producto fue eliminado");
                                                break;

                                            case 6:
                                                System.out.println("\n ║ Ingresa el SKU de un producto o alguna de las siguientes opciones:\n" +
                                                        " ║ \n" +
                                                        " ║ 1. Listar todos los productos\n" +
                                                        " ║ 2. Listar solo los productos en existencia\n" +
                                                        " ║ 3. Listar solo los productos agotados\n" +
                                                        " ║ 4. Regresar\n" +
                                                        " ║ 5. Salir");
                                                break;

                                            case 7:
                                                System.exit(1);
                                                break;

                                            default:
                                                System.out.println(" ║ Ingresa una opción válida");
                                                break;
                                        }


                                    } while (opt2 != 1 && opt2 != 2 && opt2 != 3 && opt2 != 4 && opt2 != 5 && opt2 != 6);
                                }while(opt2 != 6);

                                break;
                            }else{
                                System.out.println(" ║ Ingresa un SKU válido o alguna de las opciones listadas");
                            }
                        }

                    }

                else{
                    switch (opt) {

                        case 1:
                            for (Producto producto :
                                    productos) {
                                System.out.println(" ║ " + producto.getSku() + " | " + producto.getNombre() + " | " + producto.getDescripcion() + " | " + producto.getCantidad() + " | " + producto.getPrecio());
                            }
                            break;
                        case 2:
                            for (Producto producto :
                                    productos) {
                                if (producto.getCantidad() > 0)
                                    System.out.println(" ║ " + producto.getSku() + " | " + producto.getNombre() + " | " + producto.getDescripcion() + " | " + producto.getCantidad() + " | " + producto.getPrecio());
                            }
                            break;

                        case 3:
                            for (Producto producto :
                                    productos) {
                                if (producto.getCantidad() == 0)
                                    System.out.println(" ║ " + producto.getSku() + " | " + producto.getNombre() + " | " + producto.getDescripcion() + " | " + producto.getCantidad() + " | " + producto.getPrecio());
                            }
                            break;

                        case 4:
                            break;

                        case 5:
                            System.exit(1);
                            break;

                        default:
                            System.out.println(" ║ Ingresa una opción válida");
                            break;
                    }
                }

                } while (opt != 1 && opt != 2 && opt != 3 && opt != 4);

            } while (opt != 4);

        }

    }

}


