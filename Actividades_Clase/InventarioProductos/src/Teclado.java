import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Teclado {

    public static String LeerCadena() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String str;

        try {

            str = br.readLine();

        } catch (Exception e){

            str = "";

        }

        return str;

    }

    // Caracter

    public static char LeerCaracter() {

        char ch;

        try {

            ch = LeerCadena().charAt(0);

        } catch (Exception e){

            ch = '\0';

        }

        return ch;

    }

    // Entero

    public static int LeerEntero(){

        int num;

        try {

            num = Integer.parseInt(LeerCadena().trim());

        } catch (Exception e){

            num = 0;

        }

        return num;

    }

    public static int LeerEnteroS(){

        int num;

        Scanner entrada = new Scanner(System.in);

        num = entrada.nextInt();

        return num;

    }

    // Double

    public static double LeerDouble(){

        double num;

        try {

            num = Double.parseDouble(LeerCadena().trim());

        } catch (Exception e){

            num = 0;

        }

        return num;

    }

    // EOF

}
