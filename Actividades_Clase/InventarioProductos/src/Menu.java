public class Menu {

    public void menuPrincipal() {

        Inventario inventario = new Inventario();

        int opt;

        do {

            System.out.println("¡Bienvenido a la tiendita!, Selecciona una opción:\n" +
                    "1. Información de Productos\n" +
                    "2. Añadir Productos\n" +
                    "3. Salir\n");

            do {
                opt = Teclado.LeerEntero();

                switch (opt) {

                    case 1:
                        inventario.listarProductos();
                        break;
                    case 2:
                        inventario.anadirProducto();
                        break;

                    case 3:

                        break;

                    default:
                        System.out.println("Ingresa una opción válida");
                        break;
                }

            }while (opt != 1 && opt != 2 && opt != 3) ;

        }while (opt != 3);

        System.out.println("La ejecución del programa finalizó, ¡hasta pronto!");

    }

}