public class Producto {

    private int sku;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private double precio;

    public Producto(int sku, String nombre, String descripcion, int cantidad, double precio){

        this.sku = sku;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;

    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }

    public void addCantidad(int cantidad){

            this.cantidad+=cantidad;

    }

/*    public void subCantidad(int cantidad){

        this.cantidad-=cantidad;

}*/

    public void setPrecio(double precio){
        this.precio = precio;
    }

    public int getSku(){
        return sku;
    }

    public String getNombre(){
        return nombre;
    }

    public String getDescripcion(){
        return descripcion;
    }

    public int getCantidad(){
        return cantidad;
    }

    public double getPrecio(){
        return precio;
    }

}
