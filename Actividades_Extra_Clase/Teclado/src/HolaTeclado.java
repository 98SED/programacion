public class HolaTeclado {

    public static void main(String[] args) {

        int annio;
        double altura;
        double peso;
        char inicialApellido;

        String nombre;
        String dia;

        System.out.println("\n¿Qué día es hoy?");
        dia = Teclado.LeerCadena();
        System.out.println("\n¿Qué año es?");
        annio = Teclado.LeerEntero();
        System.out.println("\n¿Cuánto mides?");
        altura = Teclado.LeerDouble();
        System.out.println("\n¿Cuánto pesas?");
        peso = Teclado.LeerDouble();
        System.out.println("\n¿Cuál es la inicial de tu apellido?");
        inicialApellido = Teclado.LeerCaracter();
        System.out.println("\n¿Cuál es tu nombre?");
        nombre = Teclado.LeerCadena();

        System.out.println("\n¡Hola " + nombre + " " + inicialApellido + ". !, bonito " + dia + " de " + annio + " según mis registros, pesas " + peso + " [Kg] y mides " + altura + " [m] ");

    }

}
