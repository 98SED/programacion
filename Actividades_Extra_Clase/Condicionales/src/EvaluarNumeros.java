public class EvaluarNumeros {

    public static void main(String[] args) {

        // Declaración de variables:
        int cantidad;

        try {

            // Entrada de datos:
            System.out.println("¡Bienvenido Usuario!, para retirar su efectivo, ingrese una cantidad que sea múltiplo de 100:");
            cantidad = Teclado.LeerEnteroS();

            // Proceso:
            if ((cantidad % 100) == 0){

                // Salida 1:
                System.out.println("Retire su efectivo por favor");

            }else{

                // Salida 2:
                System.out.println("Las cantidades a retirar solo estan disponibles en múltiplos de 100, Ingresa una cantidad valida.\n\n");
                main(null);

            }

        }catch(Exception e){

            System.out.println("Ingresa una cantidad representada por números.\n\n");
            main(null);

        }

    }

}
