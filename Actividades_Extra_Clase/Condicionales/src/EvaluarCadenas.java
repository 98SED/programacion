public class EvaluarCadenas {

    public static void main(String[] args) {

        // Declaración de variables:
        String entrada;
        String passwdUser;

        // Entrada de datos:
        System.out.println("¡Bienvenido Usuario!, Ingresa una contraseña para iniciar sesión la próxima vez:");
        entrada = Teclado.LeerCadena();
        passwdUser = entrada;
        System.out.println("¡Cuanto tiempo!, Ingresa tu contraseña para acceder al sistema:");
        entrada = Teclado.LeerCadena();

        // Proceso:
        if (entrada.toLowerCase().equals(passwdUser.toLowerCase())){

            System.out.println("¡Bienvenido de nuevo usuario!");

        }else{

            System.out.println("Acceso denegado, la contraseña introducida es incorrecta.");

        }

    }

}
