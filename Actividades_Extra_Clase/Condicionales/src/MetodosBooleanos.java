public class MetodosBooleanos {

    public static void main(String[] args) {

        Teclado teclado = new Teclado();

        // Declaración de variables
        char letra;

        // Entrada de datos
        System.out.println(" ¡Bienvenido Usuario!, Ingresa una letra a continuación:");
        letra = teclado.LeerCaracter();

        // Proceso
        if(Character.isUpperCase(letra)){

            // Salida 1
            System.out.println("La letra (" + letra + ") introducida es una mayúscula.");

        }else if(Character.isLowerCase(letra)){

            // Salida 2
            System.out.println("La letra (" + letra + ") introducida es una minúscula.");

        }else{

            // Salida 3
            System.out.println("¡Ingresa un caracter valido!: " + letra + " no es una letra.\n\n");
            main(null);

        }

    }

}
