package com.company;

public class Constants {

    // Constantes de densidad lineal

    public static final int TEX_TITER_CONSTANT = 1000;
    public static final int DENIER_TITER_CONSTANT = 9000;
    public static final double ENGLISH_COUNT_CONSTANT_GM = 0.5906150651d;


}
