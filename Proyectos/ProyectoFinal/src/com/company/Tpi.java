package com.company;

public class Tpi implements TwistCoefficients{

    float twistCoefficient;
    double ne;
    Double tpi = null;

    public Tpi(float twistCoefficient, double ne) {
        this.twistCoefficient = twistCoefficient;
        this.ne = ne;
    }

    public Tpi(Double tpi, float twistCoefficient){
        this.tpi = tpi;
        this.twistCoefficient = twistCoefficient;
    }

    public Tpi(double ne, Double tpi){
        this.ne = ne;
        this.tpi = tpi;
    }

    public float getTwistCoefficient() {
        return twistCoefficient;
    }

    public void setTwistCoefficient(float twistCoefficient) {
        this.twistCoefficient = twistCoefficient;
    }

    public double getNe() {
        return ne;
    }

    public void setNe(double ne) {
        this.ne = ne;
    }

    public Double getTpi() {
        return tpi;
    }

    public void setTpi(Double tpi) {
        this.tpi = tpi;
    }

    public void calculateTpi(float twistCoefficient, double ne){
        setTpi(twistCoefficient * Math.sqrt(ne));
    }

    public void calculateNe(float twistCoefficient, Double tpi){
        setNe(Math.pow((tpi / twistCoefficient), 2));
    }

    public void calculateTwistCoefficient(double ne, Double tpi){
        setTwistCoefficient((float)(tpi/(Math.sqrt(ne))));
    }

}

