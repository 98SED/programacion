package com.company;

public abstract class LinearDensity {

    protected float length, mass;

    abstract public float calculateLinearDensity(float length, float mass, double constant);

}
