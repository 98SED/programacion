/**
 * El presente programa fue escrito en Java y sirve para realizar cálculos textiles básicos, especificamente,
 * la densidad lineal de los materiales textiles que representa el grosor de estos considerando sus imperfecciones
 * y las torsiones por pulgada de los hilos que en otras palabras, representan la coesión de las fibras al ser hiladas.
 */

package com.company;

public class Count extends LinearDensity implements TwistCoefficients{

    private float count = 0.0f;

    private float tpi;

    public Count(float length, float mass) {
        this.length = length;
        this.mass = mass;
    }

    public Count(float tpi) {
        this.tpi = tpi;
    }


    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public float getTpi() {
        return tpi;
    }

    public float getLength(){
        return length;
    }

    public float getMass(){
        return mass;
    }

    @Override
    public float calculateLinearDensity(float length, float mass, double constant) {

        count = ((float) constant * mass) / length;

        return count;
    }

    public final float calculateLinearDensity(float tpi, float twistCoefficient) {

        count = (float)((Math.pow(tpi, 2)) / (Math.pow(twistCoefficient, 2)));

        return count;
    }


}

