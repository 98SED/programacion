package com.company;

public interface TwistCoefficients {
    public static final float WEFT_TWIST_COEFFICIENT = 3.4f;
    public static final float WARP_TWIST_COEFFICIENT = 4.2f;
    public static final float BONNET_TWIST_COEFFICIENT = 2.4f;
    public static final float CREPE_TWIST_COEFFICIENT = 5.0f;

}
