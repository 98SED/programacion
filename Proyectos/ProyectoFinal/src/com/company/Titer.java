package com.company;

public class Titer extends LinearDensity{

    private float titer = 0.0f;

    public Titer(float length, float mass){
        this.length = length;
        this.mass = mass;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public float getTiter() {
        return titer;
    }

    public void setTiter(float titer) {
        this.titer = titer;
    }

    @Override
    public float calculateLinearDensity(float length, float mass, double constant){
        titer = ((float)constant * length) / mass;



        return titer;
    }
}
