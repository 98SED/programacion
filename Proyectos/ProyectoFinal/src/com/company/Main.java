package com.company;

public class Main {

    public static void main(String[] args) {

        int opt;
        float length, mass, tpi, twistCoefficient, count;

        System.out.println("¿Qué desea calcular?:\n" +
                "1. Número\n" +
                "2. Título\n" +
                "3. Torsiones\n");
        do {
            opt = Integer.parseInt(Entrada.LeerCadena());
            switch (opt){
                case 1:
                    System.out.println("¿Qué tipo de datos tiene para calcular el número?\n" +
                            "1. longitud y masa\n" +
                            "2. tpi\n");
                    do {
                        opt = Integer.parseInt(Entrada.LeerCadena());
                        switch (opt){
                            case 1:
                                System.out.println("Ingrese la longitud de la muestra: ");
                                length = Float.parseFloat(Entrada.LeerCadena());
                                System.out.println("Ingrese la masa de la muestra: ");
                                mass = Float.parseFloat(Entrada.LeerCadena());
                                Count ne = new Count(length, mass);
                                ne.setCount(ne.calculateLinearDensity(ne.getLength(), ne.getMass(), Constants.ENGLISH_COUNT_CONSTANT_GM));
                                System.out.println("El número de la muestra es: " + ne.getCount());

                                break;
                            case 2:
                                System.out.println("¿Qué tipo de coeficiente de torsión utilizará?\n" +
                                        "1. Estándar\n" +
                                        "2. Especializado\n");
                                do {
                                    opt = Integer.parseInt(Entrada.LeerCadena());
                                    switch (opt){
                                        case 1:
                                            System.out.println("Ingrese las tpp de la muestra: ");
                                            tpi = Float.parseFloat(Entrada.LeerCadena());
                                            Count stdNeTpi[] =  new Count[4];
                                            for (int i = 0; i < 4; i++){
                                                stdNeTpi[i] = new Count(tpi);
                                            }
                                            System.out.println("Los números calculados con coeficientes estándar son:");
                                            for (int i = 0; i < 4; i++){
                                                if ( i == 0){
                                                    stdNeTpi[i].setCount(stdNeTpi[i].calculateLinearDensity(stdNeTpi[i].getTpi(), stdNeTpi[i].WEFT_TWIST_COEFFICIENT));
                                                    System.out.println("Trama: " + stdNeTpi[i].getCount());
                                                }
                                                else if ( i == 1){
                                                    stdNeTpi[i].setCount(stdNeTpi[i].calculateLinearDensity(stdNeTpi[i].getTpi(), stdNeTpi[i].WARP_TWIST_COEFFICIENT));
                                                    System.out.println("Urdimbre: " + stdNeTpi[i].getCount());
                                                }
                                                else if ( i == 2){
                                                    stdNeTpi[i].setCount(stdNeTpi[i].calculateLinearDensity(stdNeTpi[i].getTpi(), stdNeTpi[i].BONNET_TWIST_COEFFICIENT));
                                                    System.out.println("Bonetería: " + stdNeTpi[i].getCount());
                                                }
                                                else if ( i == 3){
                                                    stdNeTpi[i].setCount(stdNeTpi[i].calculateLinearDensity(stdNeTpi[i].getTpi(), stdNeTpi[i].CREPE_TWIST_COEFFICIENT));
                                                    System.out.println("Crepé: " + stdNeTpi[i].getCount());
                                                }
                                            }
                                            break;
                                        case 2:
                                            System.out.println("Ingrese las tpp de la muestra: ");
                                            tpi = Float.parseFloat(Entrada.LeerCadena());
                                            System.out.println("Ingrese el coeficiente de torsión de la muestra: ");
                                            twistCoefficient = Float.parseFloat(Entrada.LeerCadena());
                                            Count spcNeTpi = new Count(tpi);
                                            spcNeTpi.setCount(spcNeTpi.calculateLinearDensity(spcNeTpi.getTpi(), twistCoefficient));
                                            System.out.println("El número de la muestra es: " + spcNeTpi.getCount());

                                            break;

                                    }
                                }while (opt < 1 || opt > 2);
                                break;
                        }
                    }while (opt < 1 || opt > 2);
                    break;
                case 2:
                    System.out.println("Ingrese la masa de la muestra: ");
                    mass = Float.parseFloat(Entrada.LeerCadena());
                    System.out.println("Ingrese la longitud de la muestra: ");
                    length = Float.parseFloat(Entrada.LeerCadena());
                    Titer titer1 = new Titer(length, mass);
                    System.out.println("Los títulos calculados son:\n" +
                            "Denier: " + titer1.calculateLinearDensity(titer1.getLength(), titer1.getMass(), Constants.DENIER_TITER_CONSTANT) + "\n" +
                            "Tex: " + titer1.calculateLinearDensity(titer1.getLength(), titer1.getMass(), Constants.TEX_TITER_CONSTANT) + "\n");
                    break;
                case 3:
                    System.out.println("¿Qué desea calcular?\n" +
                            "1. TPP\n" +
                            "2. Ne\n" +
                            "3. Coeficiente de Torsión");
                    do {
                        opt = Integer.parseInt(Entrada.LeerCadena());
                        switch (opt){
                            case 1:
                                System.out.println("Ingrese el nùmero de la muestra: ");
                                count = Float.parseFloat(Entrada.LeerCadena());
                                System.out.println("Ingrese el coeficiente torsión de la muestra: ");
                                twistCoefficient = Float.parseFloat(Entrada.LeerCadena());
                                Tpi tpi1 = new Tpi(twistCoefficient, count);
                                tpi1.calculateTpi(tpi1.getTwistCoefficient(), tpi1.getNe());
                                System.out.println("Las TPP de la muestra son: " + tpi1.getTpi());
                                break;
                            case 2:
                                System.out.println("Ingrese las TPP de la muestra: ");
                                tpi = Float.parseFloat(Entrada.LeerCadena());
                                System.out.println("Ingrese el coeficiente torsión de la muestra: ");
                                twistCoefficient = Float.parseFloat(Entrada.LeerCadena());
                                Tpi ne1 = new Tpi((double)tpi, twistCoefficient);
                                ne1.calculateNe(ne1.getTwistCoefficient(), ne1.getTpi());
                                System.out.println("Las TPP de la muestra son: " + ne1.getNe());
                                break;
                            case 3:
                                System.out.println("Ingrese las TPP de la muestra: ");
                                tpi = Float.parseFloat(Entrada.LeerCadena());
                                System.out.println("Ingrese el número de la muestra: ");
                                count = Float.parseFloat(Entrada.LeerCadena());
                                Tpi tC = new Tpi(count, (Double)((double)tpi));
                                tC.calculateTwistCoefficient(tC.getNe(), tC.getTpi());
                                System.out.println("Las TPP de la muestra son: " + tC.getTwistCoefficient());
                                break;
                        }


                    }while (opt < 1 || opt > 3);
                    break;
            }

        }while (opt < 1 || opt > 5);

    }
}
