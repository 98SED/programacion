package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Entrada {

    public static String LeerCadena() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String str;

        try {

            str = br.readLine();

        } catch (Exception e){

            str = "";

        }

        return str;

    }

}


